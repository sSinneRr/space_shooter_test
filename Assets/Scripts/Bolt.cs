﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bolt : MonoBehaviour
{
    public float speed;
    public float liveTime = 1;
    public void Start() {
        GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().transform.forward * speed;
        StartCoroutine(StartLive());
    }
     private IEnumerator StartLive() {
        yield return new WaitForSeconds(liveTime);
        Destroy(gameObject);
        yield break;  
    }
}
