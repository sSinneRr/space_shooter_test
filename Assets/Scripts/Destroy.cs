﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Destroy : MonoBehaviour
{
    public GameObject explosion;
    
    private GameObject cloneExplosion;
    public int damage = 1;
    public int scoreValue;
    private int healthValue = 1;
    private GameManager gameManager;
    private Health health;
    
   private void Start() {   
     GameObject GameControllerObject = GameObject.FindWithTag("GameController");
     if(GameControllerObject != null) {
       gameManager = GameControllerObject.GetComponent<GameManager>();     
    }
      GameObject HealthObject = GameObject.FindWithTag("Player");
      if(HealthObject != null) {
       health = HealthObject.GetComponent<Health>();     
    }    
   }
   private void OnTriggerEnter(Collider col) {
    if (col.tag == "Bolt") {
      cloneExplosion = Instantiate(explosion, GetComponent<Rigidbody>().position, 
          GetComponent<Rigidbody>().rotation) as GameObject;

      gameManager.AddScore(scoreValue);
      Destroy(gameObject);
      Destroy(col.gameObject);
       }
    if (col.tag == "Player") {
      cloneExplosion = Instantiate(explosion, GetComponent<Rigidbody>().position, 
           GetComponent<Rigidbody>().rotation) as GameObject;

      Destroy(gameObject);
      Destroy(cloneExplosion,1f); 

      health.TakeHit(healthValue);
       }
    } 
}
