﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
   private Health healthC;
   public Text scoreText;
   public Text missionComplite;
   private bool mission;
   public int score=0;
   public GameObject gameObject_1;
   public GameObject gameObject_2;
   public GameObject gameObject_3;
   public Menu menu;
   public static GameManager instance = null;
   int sceneIndex;
   int levelComplete;

 
   void Start() {
      sceneIndex = SceneManager.GetActiveScene().buildIndex;
      levelComplete = PlayerPrefs.GetInt("LevelComplete");

      mission = false;
      missionComplite.text = "";
      UpdateScore();  
    }

    public void isEndGame () {
        if (sceneIndex == 3) {
        Invoke("Menu", 1f);
        }
        else {
            if (levelComplete < sceneIndex) 
            PlayerPrefs.SetInt("LevelComplete", sceneIndex);
            Invoke("NextLevel", 1f);        }
    }
    void NextLevel() {
       SceneManager.LoadScene(sceneIndex +1);
    }
   void UpdateScore() {
      scoreText.text = "Наберите :300/"+score+"очков";  
    }
    public void Update() {
        if(score>=300)
          MissionC(); 
          if(Input.GetKey(KeyCode.Escape))
          SceneManager.LoadScene(0); 
    }
   public void AddScore(int newScoreValue) {//набор очков.
      score += newScoreValue;
      UpdateScore();
      }
      public void MissionC() {//условие победы.
       mission = true;
       missionComplite.text = "Nice";
       Destroy(gameObject_1);
       Destroy(gameObject_2);
       Destroy(gameObject_3);
       SceneManager.LoadScene(0);
    }
    
}
