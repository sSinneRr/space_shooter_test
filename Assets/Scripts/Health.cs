﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Health : MonoBehaviour
{
    public Text healthText;
    public int health = 3;
    public Text gameOverText;
    private bool gameOver;
    public GameObject destroy;
    public GameObject _destroy;

    private void Start() {
    gameOver = false;
    gameOverText.text ="";
    UpdateHealth();  
   }
  void UpdateHealth() {
      healthText.text = "Жизни :" + health;
    }
    public void TakeHit(int newHealthValue) {
      health -= newHealthValue;

      UpdateHealth();

      if (health <=0) {
      Destroy(gameObject);
      GameOver();
      }
 }
   public void GameOver() {
       gameOverText.text ="Game Over";
       gameOver = true;
       Destroy(destroy);
       Destroy(_destroy);
 }
}
