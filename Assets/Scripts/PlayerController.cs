﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary {
public float xMin, xMax, zMin, zMax;
}
public class PlayerController : MonoBehaviour
{
    public int helthCount;  
    public float speed;
    private Vector2 moveVelocity;
    public Boundary boundary;
    public float tilt;
    
    public Bolt shot;
    public Transform shotSpawn;

    public float fireRate =0.5f;
    private float nextFite = 0.0f;

    
    public int boltCount;
    private List<Bolt> boltPool;
   

    private void Start() {
        //пул пуль
        boltPool = new List<Bolt>(); 
        for (int i = 0; i < boltCount; i++)  {
            var boltTemp = Instantiate(shot, shotSpawn); 
            boltPool.Add(boltTemp);
            shot.gameObject.SetActive(false);
        }
    }
    //Включаем пулю и забираем из пула
    private Bolt GetBoltFromPool() {
        if (boltPool.Count > 0) {
            var boltTemp = boltPool[0];
            boltPool.Remove(boltTemp);
            boltTemp.gameObject.SetActive(true);
            boltTemp.transform.parent = null;
            boltTemp.transform.position = shotSpawn.transform.position;
            return boltTemp;
        }
        else if (boltPool.Count <= 0) { 
               for (int i = 0; i < boltCount; i++)  {
            var boltTemp = Instantiate(shot, shotSpawn); 
            boltPool.Add(boltTemp);
            shot.gameObject.SetActive(false);
        }
      }
      return null; 
    }

    private void Update() {  
        //огонь 
        if (Input.GetButton("Fire1") && Time.time > nextFite) {
            nextFite = Time.time + fireRate;
            GetBoltFromPool();
       }           
      }
    
    
    public void FixedUpdate() {
        //передвижение
         float moveHorizontal = Input.GetAxis("Horizontal");
         float moveVertical = Input.GetAxis("Vertical");
        //наклон корпуса корабля при движении
         GetComponent<Rigidbody>().rotation = Quaternion.Euler
            (0f, 0f, GetComponent<Rigidbody>().velocity.x * tilt);
        //движение корабля по вертикали и горизонтали
         GetComponent<Rigidbody>().velocity = new Vector3(moveHorizontal, 0f, moveVertical) * speed;
        //границы игровой области
         GetComponent<Rigidbody>().position = new Vector3
            (Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax), 0.0f, 
                    Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.zMin, boundary.zMax));  
    }
}

